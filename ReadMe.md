Project Goals
=============

* Implementation of the K\* algorithm by H. Aljazzar and S. Leue
* A comprehensive documentation



Documentation
=============

See [doc/build/html/index.html](doc/build/html/index.html) for a comprehensive documentation.
