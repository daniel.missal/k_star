Tutorial
========

The file *function_test.py* shows as an example how to use the *k_star()* function of this project
to find the 9 shortest paths from :math:`s_0` to :math:`s_6` in the graph of Fig. 7 in [AlLe11] (see `Literature`_).

Graph-Files
-----------

The first thing to do is to set up the files representing the problem graph.
Each graph consists of a nodes-file and an edges-file that we usually store with a *.giv* extension:

In the nodes-file each line describes exactly one node of the problem graph.
Each line must follow the pattern

  stop-id; short-name; long-name; x-coordinate; y-coordinate

where the *stop-ids* are integers starting at 1 and the lines must be sorted in ascending order
with respect to these ids.
Lines can be outcommented by a #-symbol at the beginning.

The *x*- and *y*-coordinates will make it possible to calculate the euclidean distance.
It is also possible to use scaled coordinates here (see section `Conversion-Factor`_ below).

To use a given heuristic estimate instead, enter the *h*-values as *x*-coordinates and
fill out the *y*-coordinates with zeroes.

| The corresponding nodes-file to this example is

*Stop_example_Aljazzar_Leue.giv*.

In the edges-file each line describes exactly one edge of the graph.
Here, each line must follow the pattern

  edge-id; left-stop-id; right-stop-id; length

where *length* can be regarded as a cost-value. The edges don't have to be sorted.
Lines starting with '#' will be omitted.

| The corresponding edges-file to this example is

*Edge_example_Aljazzar_Leue.giv*.

Invocation
----------

Now to use the *k_star()* function in a python script or the python command interpreter,
the following lines are mandatory:

>>> import logging
>>> from ksp import *

The following line sets up an appropriate logging mode:

>>> logging.basicConfig(filename = "k_star.log", filemode = "w", level = logging.INFO, format = "%(message)s")

Other convenient logging modes can be found in *function_test.py* or in the *HOWTO* at
`https://docs.python.org/2/howto/logging.html <https://docs.python.org/2/howto/logging.html>`_.

To finally solve the ksp-problem and store the solution paths in a path list, the *k_star()*-invocation is:

>>> path_list = k_star("Stop_example_Aljazzar_Leue.giv", "Edge_example_Aljazzar_Leue.giv", 1, 7, 1, 9)

| where "Stop_example_Aljazzar_Leue.giv" is the nodes-file name,
| "Edge_example_Aljazzar_Leue.giv is the edges-file name,
| the first 1 is the id of the start-node,
| 7 is the id of the target-node,
| the second 1 is a conversion-factor (see section `Conversion-Factor`_)
| and *k* = 9.

To sum it up, the general function syntax for *k_star()* is

>>> k_star(nodes_filename, edges_filename, start_node_id, target_node_id, conversion_factor, k)

Conversion-Factor
-----------------

The *conversion-factor* is used to treat scaled coordinates of nodes appropriately and is defined by the following equation:
  
  '`Real distance`' = '`Distance as written in .giv-file`' :math:`\cdot`
  `conversion_factor`.

If precomputed heuristic values are given, the appropriate *conversion-factor* is 1.

Extension condition
-------------------

To change the *extension condition*, consider the code block around line 310 in *a_step.py*::

      if no_of_steps_A[0] >= 1 + start_no_of_steps_A:   # Is the one-step    extension condition fulfilled?
      #if no_of_steps_A[0] >= 2 * start_no_of_steps_A:   # Is the exponential extension condition fulfilled?
        
        return 0

Here, it is possible to uncomment one of the given *extension conditions* or to create more conditions manually.

| *start_no_of_steps_A* is the number of already performed *A**-like expansions right before this A-step-invocation.
| *no_of_steps_A[0]*    is the number of already performed *A**-like expansions right now.
