Basics of K*
============

This section is supposed to give a rough overview of the functionality of the *K** algorithm.

The first thing to notice is that the expansion-process of *A** can be continued after the
shortest path from the start-node :math:`s` to the target-node :math:`t` has been found.
We call each uninterrupted sequence of such expansions an *A-step*. So the original *A** algorithm
is an A-step itself.
Whenever a node :math:`n` is expanded during an A-step, the shortest path from :math:`s` to :math:`n` is available and thus
one can create the so-called *shortest-path-tree* :math:`T`, which holds the shortest paths from :math:`s` to all nodes,
that have already been expanded.

An explored incoming edge :math:`e` of an expanded node :math:`n` in the problem graph :math:`G` is called a *sidetrack-edge*,
if it is not a part of the shortest path from :math:`s` to :math:`n`.
Let :math:`(u,v)` be a sidetrack-edge and let :math:`c(e)` denote the costs of an edge :math:`e`, than the *detour function* :math:`\delta`
is definded by :math:`\delta (u,v) \mathrel{\vcenter{:}}= g(u) + c(u,v) - g(v)` and represents the detour-costs that incur when the shortest
path from :math:`s` to :math:`u` combined with the edge :math:`(u,v)` and the shortest path from :math:`v` to :math:`t` is taken instead of
taking the shortest path from :math:`s` to :math:`t`.

After each A-step for each expanded node :math:`v` we can create the so-called *incoming-heap* of :math:`v`.
This is a binary min-heap, which consist of all the incoming sidetrack-edges of :math:`v` arranged by their :math:`\delta`-values
and satisfies the exceptional condition that its root has at most one child-node.
The root of the incoming-heap of :math:`v` is called :math:`root_{in}(v)` then.

One can also create the so-called *tree-heap* of :math:`v`. It is built as follows:
If :math:`v = s`, then the tree-heap of :math:`s` is a binary min-heap that consists solely of :math:`root_{in}(s)`.
Otherwise, let :math:`u` be the parent of :math:`v` in :math:`T`. Then the tree-heap of :math:`v` is defined to be the same as that of :math:`u`
except for the insertion of :math:`root_{in}(v)` such that the structure of a binary min-heap is maintained, where
the root can have up to two child-nodes (in comparison to incoming-heaps).
The root of the tree-heap of :math:`v` is denoted by :math:`R(v)`.

So the incoming-heaps and tree-heaps consist of incoming edges, which are considered to be the heap-nodes.
With these definitions one can finally create the so-called *path graph* :math:`PG` after each A-step.
It consists of the tree-heaps of all already expanded nodes, which are connected in the following way:

1. To each node :math:`v` in each tree-heap (which can also be considered as the root of some incoming-heap) its complete
   incoming-heap is attached.
2. For each node :math:`n` of the already constructed (possibly unconnected) graph, an outgoing edge to the tree-heap root :math:`R(u)`
   of the origin-node :math:`u` of the sidetrack-edge :math:`(u,v)` represented by :math:`n` is added to :math:`n`. This edge is called a *cross-edge*.
3. A special node :math:`\mathcal{R}` is added to the path graph together with an edge from :math:`\mathcal{R}` to :math:`R(t)`.
   We call :math:`\mathcal{R}` the root of the path graph and the added edge is called a *cross-edge* as well.

The edges which have not been labeled as cross-edges are considered to be the *heap-edges* of the path graph.
Each edge :math:`(n,n')` in the path-graph (where :math:`n` represents the sidetrack-edge :math:`e` and :math:`n'` represents :math:`e'`)
is assigned a cost :math:`\Delta(n,n')` by the following definition:

.. math::
  \begin{equation}
    \Delta(n,n') \mathrel{\vcenter{:}}=
    \begin{cases}
     \delta(e') - \delta(e) & \textrm{if} \;  (n, n') \; \textrm{is a heap-edge} \\
     \delta(e') & \textrm{if} \; (n, n') \; \textrm{is a cross-edge.}
    \end{cases}
   \end{equation}

The cross edge originating at :math:`\mathcal{R}` is assigned the :math:`\Delta`-value zero.

With these steps the construction of the path graph of the situation after the last completed A-step is finished.

To each :math:`PG`-path :math:`\sigma` starting at :math:`\mathcal{R}` one can assign a sequence :math:`\textrm{seq}(\sigma)` of sidetrack-edges in the following way:
Let :math:`n` be the last node of :math:`\sigma` and let :math:`e` be the sidetrack-edge represented by :math:`n`.
Then add :math:`e` to the yet empty sequence :math:`\textrm{seq}(\sigma)`. Now follow the path :math:`\sigma` in :math:`PG` backwards and skip all nodes until a cross-edge
is crossed. If the first node behind that cross-edge is not :math:`\mathcal{R}`, then add the sidetrack-edge represented by that node
to the sequence created so far. Continue following the :math:`PG`-path backwards and adding sidetrack-edges this way until :math:`\mathcal{R}`
is reached. Afterwards the construction of :math:`\textrm{seq}(\sigma)` is completed.

Now using this sequence of sidetrack-edges :math:`\textrm{seq}(\sigma)`, it is easy to construct a path :math:`\pi =\mathrel{\vcenter{:}} \chi(\textrm{seq}(\sigma))`
in :math:`G`: Add the target-node :math:`t` to the yet empty path :math:`\pi`.
Then, starting at the end of :math:`\textrm{seq}(\sigma)`, iterate through the sidetrack-edges backwards:
Let :math:`\rho`  be the unique path in the shortest-path-tree :math:`T` from the target of the considered sidetrack-edge to the last node added to :math:`\pi`.
Then attach :math:`\rho`  at the beginning of :math:`\pi`.
Finally, attach the unique path in :math:`T` from the start-node :math:`s` to the origin of the last considered sidetrack-edge.
This completes the construction of :math:`\pi = \chi(\textrm{seq}(\sigma))`.

Corollary 1 in [AlLe11] (see `Literature`_) states that the composition :math:`\chi \circ \textrm{seq}` is a well-defined bijection between :math:`PG`-paths starting at
:math:`\mathcal{R}` and :math:`s`-:math:`t`-paths in :math:`G`.

On the other hand, Lemma 5 in [AlLe11] (see `Literature`_) states that the length of a :math:`PG`-path :math:`\sigma` starting at :math:`\mathcal{R}` with respect to
the edge-costs :math:`\Delta` equals the total detour-cost of the sidetrack-edges in :math:`\pi = \chi(\textrm{seq}(\sigma))`.

With this information it is clear that finding the :math:`k` shortest distinguishable :math:`PG`-paths starting at :math:`\mathcal{R}` in the path graph of the
situation after expanding every single node of :math:`G` solves the ksp-problem.

[AlLe11] (see `Literature`_) also shows that in fact not all nodes of :math:`G` have to be expanded. Instead, applying A-steps on :math:`G` and *D-steps*
(i.e. expansion of nodes as in Dijkstra's algorithm starting at :math:`\mathcal{R}`) on :math:`PG` (of the current situation)
in an interleaved fashion leads to ksp-problem solutions on-the-fly.
