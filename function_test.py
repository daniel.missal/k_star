"""This module handles the testing of the `k_star()` function.

Notes
-----
To change the logger settings, see `main()`.

"""

import logging
from ksp import *



def main():
  """ This is the test-routine which calls the created `k_star()` function.
  
  Returns
  -------
  int
    This return value is always 0 unless the program is interrupted by an
    error.
  
  Notes
  -----
  To change the logger settings, see the comments in the sources.
  
  """
  
  print("")
  print("Function-Test")
  print("=============")
  print("")
  
  
  # For runtime info messages uncomment one of the following lines describing a logging mode:
  
  #logging.basicConfig(filename = "k_star.log", filemode = "w", level = logging.DEBUG, format = "%(levelname)s:%(filename)s:%(lineno)s:%(message)s")
  #logging.basicConfig(filename = None,         filemode = "w", level = logging.DEBUG, format = "%(levelname)s:%(filename)s:%(lineno)s:%(message)s")
  
  #logging.basicConfig(filename = "k_star.log", filemode = "w", level = logging.INFO,  format = "%(levelname)s:%(filename)s:%(lineno)s:%(message)s")
  #logging.basicConfig(filename = None,         filemode = "w", level = logging.INFO,  format = "%(levelname)s:%(filename)s:%(lineno)s:%(message)s")
  
  #logging.basicConfig(filename = "k_star.log", filemode = "w", level = logging.INFO,  format = "%(levelname)s:%(message)s")
  #logging.basicConfig(filename = None,         filemode = "w", level = logging.INFO,  format = "%(levelname)s:%(message)s")
  
  logging.basicConfig(filename = "k_star.log", filemode = "w", level = logging.INFO,  format = "%(message)s")
  #logging.basicConfig(filename = None,         filemode = "w", level = logging.INFO,  format = "%(message)s")
  
  #logging.basicConfig(filename = None,         filemode = "w", level = logging.ERROR, format = "%(message)s")
  
  
  ## Example from the article by H. Aljazzar and S. Leue changed to monotonicity
  ## ---------------------------------------------------------------------------
  
  path_list1 = k_star("Stop_example_Aljazzar_Leue.giv", "Edge_example_Aljazzar_Leue.giv", 1, 7, 1, 9)
  #path_list1 = k_star("Stop_example_Aljazzar_Leue.giv", "Edge_example_Aljazzar_Leue.giv", 1, 7, 1, 127)        # The 123th path found should be the shortest one using S5.
  print("\nResult paths using K*:\n")
  counter = 1
  for path in path_list1:
    print("%i.) (Length = %f)" % (counter, path_length(path)))
    print_path(path, False)
    counter += 1
  
  
  # Example GoeVB (needs conversion factor of 0.001) in the heuristic estimate
  # --------------------------------------------------------------------------
  
  #path_list2 = k_star("Stop_GoeVB.giv", "Edge_GoeVB.giv", 105, 63,  0.001, 1000)                               # Hagenbreite to Eschenbreite
  #path_list2 = k_star("Stop_GoeVB.giv", "Edge_GoeVB.giv", 138, 139, 0.001, 9)
  #path_list2 = k_star("Stop_GoeVB.giv", "Edge_GoeVB.giv", 111, 161, 0.001, 9)
  #print("\nResult paths using K*:\n")
  #counter = 1
  #for path in path_list2:
    #print("%i.) (Length = %f)" % (counter, path_length(path)))
    #print_path(path, False)
    #counter += 1
  
  
  # Monotonic Example
  # -----------------
  
  #graph = assemble_whole_graph("Stop_example_monotonic.giv", "Edge_example_monotonic.giv")
  #path_list3 = k_star("Stop_example_monotonic.giv", "Edge_example_monotonic.giv", 1, 5, 1, 10)
  #print("\nResult paths using K*:\n")
  #counter = 1
  #for path in path_list3:
    #print("%i.) (Length = %f)" % (counter, path_length(path)))
    #print_path(path, False)
    #counter += 1
  
  return 0



if __name__ == "__main__":
  main()
