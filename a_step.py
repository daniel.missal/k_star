"""This module handles the performance of A-steps.

Notes
-----
An A-step is a complete run of the original A* algorithm or the
continuation of A*-expansions after the target-node has already been
expanded by the original A* algorithm. In the latter case one A-step can
comprise multiple A*-like expansions. This depends on the form of the
extension condition in `perform_A_step()`. So each A-step gives rise to an
expansion of the path graph, which is rendered by the functions of the
`pg` module.

The open list used for the A*-like expansions consists of
`Copen_list_A_obj` objects, which include a node v, the length g of the
shortest path from the start-node to v found so far and the so far expected
lower boundary f for the length of the shortest path from the start-node to
the target-node via v. The list is always sorted by f in ascending order.
The closed list only consists of `Cnode` objects though. It is not sorted
at all.

Notation: olA means 'open list A-step' while olD means 'open list D-step'.

"""

import logging
import math
from pg import *



class Copen_list_A_obj:
  """This class represents the elements of the open list used in A-steps.
  
  Attributes
  ----------
  node : Cnode
    Pointer to the node object of the node that this element represents
  g : float
    Length of the shortest path found so far from the start-node to the
    represented node
  f : float
    So far expected lower boundary for the length of the shortest path from
    the start-node to the target-node via the represented node
  
  """
  
  def __init__(self, node, g, f):
    """The constructor just assigns all attributes.
    
    Parameters
    ----------
    node : Cnode
      Pointer to the node object of the node that this element represents
    g : float
      Length of the shortest path found so far from the start-node to the
      represented node
    f : float
      So far expected lower boundary for the length of the shortest path
      from the start-node to the target-node via the represented node
    
    """
    
    self.node = node
    self.g    = g
    self.f    = f


def h(node, target_node, conversion_factor):
  """This function calculates the (2-dimensional) euclidean distance
  between a given node and the target-node.
  
  Parameters
  ----------
  node : Cnode
    The considered node
  target_node : Cnode
    The target-node of the ksp-problem
  conversion_factor : float
    A conversion-factor as described below
  
  Returns
  -------
  float
    The (2-dimensional) euclidean distance between the considered node and the target-node of the ksp-problem
  
  Notes
  -----
  This function is used as a heuristic estimate for the distance of a node
  to the target-node of a shortest-path-problem or k-shortest-path-problem.
  
  For large distances between stops the 2-dimensional simplification of the
  earth's surface might cause noticeable deviations.
  
  In case the target-node of a ksp-problem is fixed and the used nodes-file
  is supposed to contain precalculated heuristic estimates with respect to
  said target-node, the nodes-file should be set up in a way, that each
  `x`-coordinate is set to the given h-value and each `y`-coordinate is set
  to zero. Then with the `conversion_factor` set to 1 this function still
  returns the correct h-value as all computations cancel each other out.
  
  The `conversion_factor` is defined by the following equation:
  
  '`Real distance`' = '`Distance as written in .giv-file`' :math:`\cdot`
  `conversion_factor`
  
  """
  
  return math.sqrt(((node.x - target_node.x)**2 + (node.y - target_node.y)**2)) * conversion_factor


def insert_into_olA(open_list_A, node_to_expand, explored_node, g, f):
  """This function inserts a new entry into the open list for A*-like
  expansions.
  
  More specifically, a new `Copen_list_A_obj` object is created and
  inserted into the open list.
  
  olA means 'open list A-step'
  
  Parameters
  ----------
  open_list_A : list
    The open list for A-steps
  node_to_expand : Cnode
    The node being expanded when the node to insert was explored
  explored_node : Cnode
    The node to be inserted into the open list as a `Copen_list_A_obj`
    object
  g : float
    Length of the shortest path found so far from the start-node to the
    node to insert
  f : float
    So far expected lower boundary for the length of the shortest path from
    the start-node to the target-node via the node to insert  
  
  """
  
  explored_node.predecessor = node_to_expand
  
  
  # Find position in open_list_A to insert:
  
  insert_index = 0
  
  for element in open_list_A:
    
    if element.f < f:
      insert_index += 1
    else:
      break
  
  
  # Insert the node into the open_list_A:
  
  open_list_A.insert(insert_index, Copen_list_A_obj(explored_node, g, f))


def perform_A_step(nodes, start_node, target_node, conversion_factor, open_list_A, closed_list_A, no_of_steps_A, find_target_mode, refresh_list):
  """This function performs an A-step.
  
  Parameters
  ----------
  nodes : list
    The list of all nodes of the original graph G held by K*
  start_node : Cnode
    The start-node of the ksp-problem
  target_node : Cnode
    The target-node of the ksp-problem
  conversion_factor : float
    A conversion factor as described in the notes for `h()`
  open_list_A : list
    The open list for A-steps
  closed_list_A : list
    The closed list for A-steps
  no_of_steps_A : int
    The number of expansions that have already been performed during
    A-steps
    
    Note that this is not the number of A-steps performed.
    One A-step can include several expansions.
    
    If `find_target_mode` is True, expansions are performed until the
    target is found.
    
    If `find_target_mode` is False, the number of expansions depends on the
    extension condition.
  find_target_mode : bool
    The type of A-step to be run
    
    If `find_target_mode` is True, a complete run of the original
    A*-algorithm is performed to find the target-node. This is done only at
    the beginning of K*.
    
    If `find_target_mode` is False, the continuation of A*-expansions is
    performed until the extension condition is fulfilled.
  refresh_list : list
    The refresh_list of K*
    
    When new sidetrack-edges are explored during an A-step which is not in
    `find_target_mode`, the corresponding `Cpg_node` object is placed on
    the `refresh_list`, so that later in `refresh_olD()` (see the
    `d_step` module) new successors of already expanded path graph nodes
    can be added to the open list for D-steps.
  
  Notes
  -----
  What type of A-step (a complete original A*-run or just the continuation
  of A*-expansions) is performed, depends on the parameter
  `find_target_mode`.
  
  To change the extension condition or to choose a predefined one, see the
  end of the main loop of this function.
  
  """
  
  start_no_of_steps_A = no_of_steps_A[0]
  
  while open_list_A:                            # While open_list_A is not empty...
    
    logging.info("\nOpen List of 'A*':")
    logging.info("")
    logging.info(" f                   g                   node")
    logging.info(" ------------------------------------------------------------------------------")
    for element in open_list_A:
      logging.info(" %8.3f            %8.3f            %s" % (element.f, element.g, element.node.long_name))
    logging.info("")
    
    element_to_expand = open_list_A.pop(0)              # Get first entry of open list
    
    node_to_expand = element_to_expand.node
    g              = element_to_expand.g
    
    logging.info("Expanding node %s\n" % node_to_expand.long_name)
    
    node_to_expand.g = g
    
    build_incoming_heap(node_to_expand)
    print_incoming_heap(node_to_expand)
    
    
    # Expand node:
    
    for edge in node_to_expand.outgoing_edges:
      
      explored_node = edge.right_stop
      
      logging.info(" Exploring node %s\n" % explored_node.long_name)
      
      if explored_node.closed:                                          # Has the end of the edge already been expanded?
        
        pg_node = update_incoming_heap(explored_node, edge)
        
        print_incoming_heap(explored_node)
        
        if not find_target_mode:                                                # Some nodes in the path graph might have just gotten a new successor,
          refresh_list.append(pg_node)                                          # although they have already been expanded in a D-step
        
        continue
      
      else:
        explored_node.incoming_edges.append(edge)
      
      length = edge.length
      
      
      # Check if the explored node is alreay in the open_list_A:
      
      found       = False
      found_index = 0
      
      for element in open_list_A:
        
        if element.node == explored_node:
          
          found         = True
          found_element = element
          
          break
        
        found_index += 1
      
      f = g + length + h(explored_node, target_node, conversion_factor)
      
      if not found:                                                                     # If the node has not been explored yet...
        
        insert_into_olA(open_list_A, node_to_expand, explored_node, g + length, f)
      
      else:                                                                             # If the node is already on the open_list_A...
        
        if f < found_element.f:                                                                 # If a shorter path towards the explored node was just found...
          
          del open_list_A[found_index]                                                                  # Remove the old open_list_A entry
          
          insert_into_olA(open_list_A, node_to_expand, explored_node, g + length, f)
    
    closed_list_A.append(node_to_expand)         # Place the expanded node on the closed_list_A
    node_to_expand.closed = True
    
    no_of_steps_A[0] += 1
    
    if find_target_mode:
      
      if node_to_expand == target_node:                 # Is it already the target node?
        return 0
      
    else:
      
      # Check the extension condition:
      
      if no_of_steps_A[0] >= 1 + start_no_of_steps_A:   # Is the one-step    extension condition fulfilled?
      #if no_of_steps_A[0] >= 2 * start_no_of_steps_A:   # Is the exponential extension condition fulfilled?
        
        return 0
    
  return 1      # This is only executed, if no path to the target_node has been found.
