"""This module constitutes the head of the K* algorithm.

It comprises the main function of K* and the function `maximum_d()`. The
latter returns the maximum of all successor distances, which is an
important little step of K*, but a little more extensive in the pointerless
array implementation of the heaps used here. The function `maximum_d()` is
invoked at exactly one site of K* and only implemented as a stand-alone
function to increase the readability of the `k_star()` function at the
bottom.

"""

import logging
from g      import *
from a_step import *
from d_step import *



def maximum_d(open_list_D, target_node):
  """This function returns the maximum of the distances between each
  successor of the current head node of the open list for D-steps and the
  PG-root.
  
  Parameters
  ----------
  open_list_D : list
    The open list for D-steps
  target_node : Cnode
    The target-node of the ksp-problem
  
  Returns
  -------
  float
    The maximum of the distances between each successor of the current head
    node of the open list for D-steps and the PG-root
  
  """
  
  maximum = 0

  n_element = open_list_D[0]

  n_type    = n_element.pg_node_type

  if n_type == "pg_root":
    
    heap = target_node.tree_heap
    
    if len(heap) >= 2:                                                  # Note the dummy element at position 0 of all tree-heaps
      
      maximum = heap[1].delta
  
  else:
    
    n_heap_owner = n_element.heap_owner
    n_pos        = n_element.pos
    n_d          = n_element.d
    
    if n_type == "inc_root":
      
      heap         = n_heap_owner.tree_heap
      n            = heap[n_pos]
      
      i_heap       = n.right_stop.inc_heap                                      # Incoming-heap that n is a part of
      
      if len(i_heap) >= 2:
        
        maximum = n_d + i_heap[1].delta - n.delta
      
    else: # Here we have n_type == "non_inc_root"
      
      heap         = n_heap_owner.inc_heap
      n            = heap[n_pos]
    
    c_heap       = n.left_stop.tree_heap                                # Target tree-heap of outgoing cross edge
    
    if len(c_heap) >= 2:
      
      maximum = max(maximum, n_d + c_heap[1].delta)
    
    length = len(heap)
    
    if length >= 2 * n_pos + 1:                                         # Check child_a
      
      maximum = max(maximum, n_d + heap[2 * n_pos].delta - n.delta)
      
      if length >= 2 * n_pos + 2:                                               # Check child_b
        
        maximum = max(maximum, n_d + heap[2 * n_pos + 1].delta - n.delta)
  
  return maximum


def k_star(nodes_filename, edges_filename, start_node_id, target_node_id, conversion_factor, k):
  """This function returns a list containing the k shortest paths from a
  start-node to a target-node.
  
  Parameters
  ----------
  nodes_filename : str
    String containing the file name of the .giv-file containing the nodes
    
    Each line of this file must obey the pattern
    
    stop-id; short-name; long-name; x-coordinate; y-coordinate
    
    to describe exactly one node.
    Lines starting with '#' will be omitted.
    The list must be sorted by the node-ids in ascending order.
  edges_filename : str
    String containing the file name of the .giv-file containing the edges
    
    Each line of this file must obey the pattern
    
    edge-id; left-stop-id; right-stop-id; length
    
    to describe exactly one edge.
    Lines starting with '#' will be omitted.
    This function does not expect the lines to be sorted by left-stop-id
    and does not benefit from it in any way.
  start_node_id : int
    Id of the start-node of the ksp-problem
  target_node_id : int
    Id of the target-node of the ksp-problem
  conversion_factor : float
    A conversion factor as defined in the description of `h()` in the
    `a_step` module
  k : int
    Positive integer describing how many of the shortest paths to return
    
    Example: `k` = 1 makes K* basically equal A* and return only the
    shortest path while `k` = 3 makes K* return the shortest, then
    2nd-shortest and the 3rd-shortest path.
  
  Returns
  -------
  list
    A list of the `k` shortest paths from the start-node to the target-node
    of the specified graph.
  
  Notes
  -----
  This function is the head of the K* algorithm to solve the
  k-shortest-paths (short: ksp) problem. Cycles in a path are explicitly
  allowed and distinguish a path from itself without its cycles.
  
  If there is no path from the start-node to the target-node, then an empty
  list is returned.
  
  If there are less than `k` different paths from the start-node to the
  target-node, then only those will be included in the returned list. If
  different paths have the same length, then the order of their appearance
  in the result list depends on the implementation. Depending on the given
  graph G and `k` some paths may be not included, although they are as
  short as other paths which are included in the result list. This can
  happen, if `k` interrupts a sequence of paths of the same length.
  
  Example: Shortest paths[length]: A[7], B[8], C[8], D[8], where `k` = 3
  makes D not appear.
  
  """
  
  nodes = [0]                                                                           # First entry is a dummy entry to make list indices and node-ids equal
  
  assemble_whole_graph(nodes_filename, edges_filename, nodes)
  
  start_node  = nodes[start_node_id]
  target_node = nodes[target_node_id]
  
  open_list_A   = [Copen_list_A_obj(start_node, 0, 0)]
  closed_list_A = []
  
  no_of_steps_A = [0]
  
  open_list_D   = []
  closed_list_D = []
  
  refresh_list  = []
  
  extension_result = perform_A_step(nodes, start_node, target_node, conversion_factor, open_list_A, closed_list_A, no_of_steps_A, True, refresh_list)
  
  if extension_result == 1:
    
    logging.warning("There is no path from %s to %s." % (start_node.long_name, target_node.long_name))
    
    return []
  
  g_target = target_node.g
  
  no_of_extensions = 1
  
  reconstruct_tree_heaps(closed_list_A, start_node, no_of_extensions)
  
  pg_root = Cpg_node(target_node, None, 0)                              # This line also sets up the cross edge from the pg_root to the tree-heap of the target-node.
  
  open_list_D.append(CD_list_obj("pg_root", None, -1, 0, None, False))  # Add pg_root to open-list
  
  path_list = []
  
  while len(path_list) < k:
    
    if open_list_A and open_list_D:
      
      logging.debug("Case A\n")
      
      # Evaluate the scheduler condition:
      
      maximum = maximum_d(open_list_D, target_node)
      f       = open_list_A[0].f
      
      logging.info("Evaluating the scheduler condition:")
      
      if g_target + maximum <= f:
        
        logging.debug(" Case A-1:")
        logging.info("g(t) + d = %f + %f = %f <= %f = f(u).\n" % (g_target, maximum, g_target + maximum, f))
        
        perform_D_step(open_list_D, closed_list_D, start_node, target_node, path_list)
      
      else:
        
        logging.debug(" Case A-2:")
        logging.info("g(t) + d = %f + %f = %f > %f = f(u).\n"  % (g_target, maximum, g_target + maximum, f))
        
        perform_A_step(nodes, start_node, target_node, conversion_factor, open_list_A, closed_list_A, no_of_steps_A, False, refresh_list)
        
        no_of_extensions += 1
        
        reconstruct_tree_heaps(closed_list_A, start_node, no_of_extensions)
        
        refresh_olD(refresh_list, open_list_D, closed_list_D, target_node)
    
    elif open_list_A and not open_list_D:
      
      logging.debug("Case B")
      
      perform_A_step(nodes, start_node, target_node, conversion_factor, open_list_A, closed_list_A, no_of_steps_A, False, refresh_list)
      
      no_of_extensions += 1
      
      reconstruct_tree_heaps(closed_list_A, start_node, no_of_extensions)
      
      refresh_olD(refresh_list, open_list_D, closed_list_D, target_node)
    
    elif not open_list_A and open_list_D:
      
      logging.debug("Case C")
      
      perform_D_step(open_list_D, closed_list_D, start_node, target_node, path_list)
    
    elif not open_list_A and not open_list_D:
      
      logging.debug("Case D")
      
      break                                                     # Terminate
  
  return path_list
